package ru.itis.taxi.repositories;

import ru.itis.taxi.models.User;

import java.io.*;
import java.util.*;
import java.util.function.Function;

public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;
    private List<User> temporaryMemory;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();
    private String id;
    private User tempUser;

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
        this.temporaryMemory = new ArrayList<User>();
    }

    @Override
    public List<User> findAll() {
        saveChangedDataInTempMemory(x -> x, null);
        List<User> answer = temporaryMemory;
        temporaryMemory = new ArrayList<>();
        return answer;
    }

    @Override
    public void save(User entity) {
        String id = UUID.randomUUID().toString();
        entity.setId(id);
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        saveChangedDataInTempMemory(x -> new User(x.getId(),
                        entity.getFirstName(),
                        entity.getLastName(),
                        entity.getEmail(),
                        entity.getPassword()),
                entity.getId());
        writeChangedDataInFile();
    }

    @Override
    public void delete(User entity) {
        saveChangedDataInTempMemory(x ->
                new User("X","X","X","X"), entity.getId());
        writeChangedDataInFile();
    }

    @Override
    public void deleteById(String id) {
        saveChangedDataInTempMemory( x ->
                new User("X","X","X","X"), id);
        writeChangedDataInFile();
    }

    @Override
    public User findById(String id) {
        saveChangedDataInTempMemory(x -> {
            tempUser = x;
            return  x;
        }, id);
        User user = tempUser;
        tempUser = null;
        temporaryMemory = new ArrayList<User>();
        return user;
    }

    private void saveChangedDataInTempMemory(Function<User,User> function, String id) {
        try (Reader reader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(reader)) {
            while (bufferedReader.ready()) {
                String[] data = bufferedReader.readLine().split("\\|");
                User user = new User(data[0], data[1], data[2], data[3], data[4]);
                if (user.getId().equals(id))
                    user = function.apply(user);
                temporaryMemory.add(user);
            }
        }  catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void writeChangedDataInFile() {
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            cleanFile();
            for (User us : temporaryMemory) {
                if (us.getId() != null) {
                    bufferedWriter.write(userToString.apply(us));
                    bufferedWriter.newLine();
                }
            }
            bufferedWriter.close();
            temporaryMemory = new ArrayList<User>();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void cleanFile() {
        try (Writer writer = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            bufferedWriter.write("");
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
