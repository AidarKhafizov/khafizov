package ru.itis.taxi.app;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.mappers.Mappers;
import ru.itis.taxi.repositories.UsersRepository;
import ru.itis.taxi.repositories.UsersRepositoryFilesImpl;
import ru.itis.taxi.services.UsersService;
import ru.itis.taxi.services.UsersServiceImpl;

import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        System.out.println(UUID.randomUUID().toString());
//        UsersRepository usersRepository = new UsersRepositoryInMemoryImpl();
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));

        int i = 0;
        List<User> allUsers = usersService.findAll();
        User wrongNameUser = new User("***", "***", "***", "***");
        wrongNameUser.setId(allUsers.stream().findFirst().get().getId());
        usersService.updateAccount(wrongNameUser);
        User last = allUsers.get(1);
        if (allUsers.stream().count() != 2)
            throw  new InvalidObjectException("User's numbers is ircorrect");
        User check = usersService.findById(last.getId());
        if (!check.getFirstName().equals(last.getFirstName()) ||
                !check.getEmail().equals(last.getEmail()) ||
                !check.getLastName().equals(last.getLastName()))
            throw  new IOException();
        usersService.deleteAccountById(last.getId());
        allUsers = usersService.findAll();
        if (allUsers.stream().count() != 1)
            throw  new InvalidObjectException("User's numbers is ircorrect");
        User single = allUsers.get(0);
        usersService.deleteAccount(single);
        allUsers = usersService.findAll();
        if (allUsers.stream().count() != 0)
            throw  new InvalidObjectException("User's numbers is ircorrect");

    }
}

