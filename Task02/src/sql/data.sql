insert into client(first_name, last_name, rating)
values ('Ильнур', 'Фазизун', 1);
insert into client(id,first_name, last_name, rating)
values ('Илья', 'Гребень', 0);
insert into client(id,first_name, last_name, rating)
values ('Даврон', 'Мамаев', 10);

update client
set phone_number = '89273475555',
    rating       = 2
where id = 1;

insert into driver(id,first_name, last_name, rating, experience)
values (1,'Александр','Александров',1,2),
       (2,'Тихон','Глухарев',3,7),
       (3,'Данил','Исатов',5,10);

update driver
set car_id = 2
where id = 2;

update driver
set car_id = 1
where id = 1;

update driver
set car_id = 3
where id = 3;

insert into car(id, brand, color, number)
values (1, 'Kia ceed', 'red', 'с700мр'),
       (2, 'Lada vesta', 'black', 'е201ат'),
       (3, 'Kia k5', 'blue', 'е901аа');

insert into ride(id, driver_id, client_id, start_point, final_point)
values (1, 1, 2, 'Ленина 8', 'Строительная 72')
       (2, 2, 3, 'Островского 76', 'Ленина 66'),
       (3, 3, 1, 'Шашина 3', 'Северная 133');
